export const generateAssetStatus = new Map([
  ['stopped', '#E30E0E'],
  ['maintenance', '#E4AE25'],
  ['running', '#37B744']
])
