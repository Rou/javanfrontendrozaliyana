export interface IUser {
  id: number;
  fullname: string;
  username: string;
  password: string;
  role: string;
}
