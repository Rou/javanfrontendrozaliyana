export interface IConfiguration {
  id: number;
  configName: string;
  type: string;
  details: string;
}
