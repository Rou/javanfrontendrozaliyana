import { IAsset } from "../../../assets/shared/model/asset.model";

export const DBAssetInactiveReview: IAsset[] = [
  {
    id: 1008,
    assetName: "MacBook 13",
    serialNumber: "MACSN20232222",
    type: "Laptop",
    locationId: 1,
    modelNumber: "A5112",
    manufacturer: "Apple",
    currentStatus: "Running",
    downtime: 90
  },
  {
    id: 1009,
    assetName: "MacBook 15",
    serialNumber: "MACSN20233333",
    type: "Laptop",
    locationId: 1,
    modelNumber: "A5555",
    manufacturer: "Apple",
    currentStatus: "Running",
    downtime: 50
  },
  {
    id: 1010,
    assetName: "Samsung S22",
    serialNumber: "SAM00001",
    type: "Handphone",
    locationId: 2,
    modelNumber: "S-Series-22",
    manufacturer: "Samsung",
    currentStatus: "Running",
    downtime: 30
  },
  {
    id: 1004,
    assetName: "Dell 28Inc",
    serialNumber: "DELLSN-0001111",
    type: "Monitor",
    locationId: 1,
    modelNumber: "DELL MNTR001",
    manufacturer: "Dell Inc",
    currentStatus: "Running",
    downtime: 25
  },
  {
    id: 1005,
    assetName: "Dell 32Inc",
    serialNumber: "DELLSN-0002222",
    type: "Monitor",
    locationId: 1,
    modelNumber: "DELL MNTR002",
    manufacturer: "Dell Inc",
    currentStatus: "Running",
    downtime: 20
  },
  {
    id: 1007,
    assetName: "MacBook 15",
    serialNumber: "MACSN20231111",
    type: "Laptop",
    locationId: 1,
    modelNumber: "A5102",
    manufacturer: "Apple",
    currentStatus: "Running",
    downtime: 30
  }
]
