import { IAsset } from "../../../assets/shared/model/asset.model";

export const DBAssetReview: IAsset[] = [
  {
    id: 1001,
    assetName: "Thinkpad Laptop LTS1000",
    serialNumber: "SN-00001",
    type: "Laptop",
    locationId: 1,
    modelNumber: "XESAP01",
    manufacturer: "Lenovo",
    currentStatus: "Stopped"
  },
  {
    id: 1002,
    assetName: "HP Printer",
    serialNumber: "SN-000ABC111",
    type: "Printer",
    locationId: 2,
    modelNumber: "CCSAP02",
    manufacturer: "Hewlett Packard",
    currentStatus: "Maintenance",
  },
  {
    id: 1003,
    assetName: "Dell Monitor 26Inc",
    serialNumber: "DELLSN-0009090",
    type: "Monitor",
    locationId: 2,
    modelNumber: "DELL XPS01",
    manufacturer: "Dell Inc",
    currentStatus: "Stopped"
  },
  {
    id: 1004,
    assetName: "Dell Monitor 28Inc",
    serialNumber: "DELLSN-0001111",
    type: "Monitor",
    locationId: 1,
    modelNumber: "DELL MNTR001",
    manufacturer: "Dell Inc",
    currentStatus: "Maintenance",
  },
  {
    id: 1005,
    assetName: "Dell Monitor 32Inc",
    serialNumber: "DELLSN-0002222",
    type: "Monitor",
    locationId: 1,
    modelNumber: "DELL MNTR002",
    manufacturer: "Dell Inc",
    currentStatus: "Stopped"
  },
  {
    id: 1006,
    assetName: "Dell Monitor 32Inc",
    serialNumber: "DELLSN-0002222",
    type: "Monitor",
    locationId: 1,
    modelNumber: "DELL MNTR002",
    manufacturer: "Dell Inc",
    currentStatus: "Stopped"
  }
]
