export interface ISparepart {
  id: number;
  sparepartName : string;
  quantity: number;
  type: string;
  locationId: number;
  modelNumber: string;
  manufacturer: string;
  currentStatus: string;
}
