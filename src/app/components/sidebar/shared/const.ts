import { ISidebar } from "./sidebar.model";

export const sidebarNavigations: ISidebar[]= [
  {
    name: 'Assets',
    path: 'assets'
  },
  {
    name: 'Spare parts',
    path: 'spareparts'
  },
  {
    name: 'Configurations',
    path: 'configurations'
  }
]
