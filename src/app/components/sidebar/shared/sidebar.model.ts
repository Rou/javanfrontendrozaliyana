export interface ISidebar {
  name: string;
  path: string;
}
