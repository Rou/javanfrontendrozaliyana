export const avatar = './assets/avatar/migdalia.png';

export const image = {
  gallery: {
    src: './assets/image/gallery.svg',
    alt: 'gallery'
  }
}
export const icons = {
  home:{
    src: '/assets/icons/home.svg',
    alt: 'home'
  },
  logout: {
    src: '/assets/icons/logout.svg',
    alt: 'logout'
  },
  sort: {
    src: '/assets/icons/sort.svg',
    alt: 'sort'
  },
  download: {
    src: '/assets/icons/download.svg',
    alt: 'sort'
  },
  close : {
    src: '/assets/icons/close.svg',
    alt: 'sort'
  }
}
